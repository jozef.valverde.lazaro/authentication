package rest

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/auth"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/domain"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/jwtauth"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/mongodb"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/redis"
	"gitlab.com/jozef.valverde.lazaro/authentication/utils/errorsutils"
	"gitlab.com/jozef.valverde.lazaro/authentication/utils/responseutils"
)

//Router represents router
type Router struct {
	engine *gin.Engine
}

// NewRouter instantiate new router
func NewRouter(engine *gin.Engine) *Router {
	return &Router{
		engine: engine,
	}
}

// URLMapping map enpoints
func (r *Router) URLMapping() {
	/*r.engine.POST("/auth", createToken)
	r.engine.GET("/auth/:username", retrieveToken)*/
	r.engine.POST("/login", login)
	r.engine.Run()
}

func login(c *gin.Context) {
	var attemptLogin domain.UserLogin

	if err := c.ShouldBindJSON(&attemptLogin); err != nil {
		c.JSON(http.StatusBadRequest, errorsutils.NewBadRequestError("invalid json body"))
		return
	}
	fmt.Println(attemptLogin)
	mongoService := mongodb.NewService()
	jwtauthService := jwtauth.NewService(mongoService)
	token := jwtauthService.GenerateJWT(&attemptLogin)
	c.JSON(http.StatusOK, responseutils.NewTokenLoginResponse(token, ""))

}

func createToken(c *gin.Context) {
	var attemptLogin domain.AttemptLogin

	if err := c.ShouldBindJSON(&attemptLogin); err != nil {
		c.JSON(http.StatusBadRequest, errorsutils.NewBadRequestError("invalid json body"))
		return
	}
	redisConfig := redis.Config{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	}
	redisService := redis.NewService(&redisConfig)
	authService := auth.NewService(redisService)
	authToken, err := authService.GenerateToken(&attemptLogin)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorsutils.NewInternalServerError("redis internal server error"))
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": &authToken.Token})
}

func retrieveToken(c *gin.Context) {
	redisConfig := redis.Config{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	}
	username := strings.TrimSpace(c.Param("username"))
	redisService := redis.NewService(&redisConfig)
	authService := auth.NewService(redisService)
	authToken, err := authService.ConsultTokenByUsername(username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorsutils.NewInternalServerError("redis internal server error"))
		return
	}
	if authToken == "" {
		c.JSON(http.StatusNotFound, errorsutils.NewNotFoundError("not found"))
		return
	}
	c.JSON(http.StatusOK, gin.H{"token": authToken})
}
