package jwtauth

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/jozef.valverde.lazaro/authentication/internal/domain"
)

// Service define jwtauth service
type Service struct {
	mongoService domain.MongoServiceInteface
}

// NewService instantiate jwtauth service
func NewService(mongoService domain.MongoServiceInteface) *Service {
	return &Service{
		mongoService: mongoService,
	}
}

//GenerateJWT generate json web token
func (s *Service) GenerateJWT(userlogin *domain.UserLogin) string {
	fmt.Println(userlogin)
	userFound, err := s.mongoService.GetUserByUsername(userlogin.Username)
	if err != nil {
		panic(err)
	}
	if userFound.Firstname == "" {
		fmt.Printf("user not found")
	}
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = userFound.Firstname
	claims["admin"] = true
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	//generate encoded token
	tokenEncoded, err := token.SignedString([]byte("secret"))
	if err != nil {
		panic(err)
	}
	return tokenEncoded
}
