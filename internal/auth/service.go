package auth

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/domain"
	"gitlab.com/jozef.valverde.lazaro/authentication/utils/cryptoutils"
)

//Service represents auth service
type Service struct {
	redis domain.RedisServiceInterface
}

// NewService instantiate auth service
func NewService(redis domain.RedisServiceInterface) *Service {
	return &Service{
		redis: redis,
	}
}

//GenerateToken genterates token
func (s *Service) GenerateToken(attemptLogin *domain.AttemptLogin) (*domain.AuthToken, error) {

	input := fmt.Sprintf("%s%s%v", attemptLogin.Username, attemptLogin.Password, time.Now().Unix())

	token := &domain.AuthToken{
		Username: attemptLogin.Username,
		Token:    cryptoutils.Encrypt(input),
	}
	if err := s.redis.PersistToken(token); err != nil {
		return nil, err
	}
	return token, nil
}

//ConsultTokenByUsername get token
func (s *Service) ConsultTokenByUsername(username string) (string, error) {
	token, err := s.redis.RetrieveToken(username)
	if err == redis.Nil {
		return "", nil
	}
	if err != nil {
		return "", err
	}
	return token, nil
}
