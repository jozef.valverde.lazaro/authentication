package domain

// UserFindMongo represent user find by mongo client
type UserFindMongo struct {
	Username  string `json:"username"`
	Email     string `json:"email"`
	Firstname string `json:"first_name"`
	Lastname  string `jsom:"last_name"`
}

//MongoServiceInteface represents mongoservice
type MongoServiceInteface interface {
	GetUserByUsername(username string) (*UserFindMongo, error)
}
