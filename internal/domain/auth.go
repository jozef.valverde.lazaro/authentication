package domain

//AttemptLogin represents the request to ask for generating token
type AttemptLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//AuthToken represents a token generated
type AuthToken struct {
	Username string `json:"username"`
	Token    string `json:"token"`
}

//AuthServiceInteface represents the auth service
type AuthServiceInteface interface {
	GenerateToken(*AttemptLogin) (*AuthToken, error)
	ConsultToken(string) (string, error)
}
