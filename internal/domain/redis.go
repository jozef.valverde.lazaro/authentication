package domain

//RedisServiceInterface represents redis service
type RedisServiceInterface interface {
	PersistToken(*AuthToken) error
	RetrieveToken(string) (string, error)
}
