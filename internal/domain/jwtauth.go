package domain

//UserLogin represents info of user login
type UserLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//TokenLoginResponse represents reponse body of login
type TokenLoginResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

// JwtauthServiceInterface represents jwt auth service
type JwtauthServiceInterface interface {
	GenerateJWT(*UserLogin) string
}
