package redis

// Config is de configuration for redis library
type Config struct {
	Addr     string `default:"localhost:6379"`
	Password string `default:""`
	DB       int    `default:"0"`
}
