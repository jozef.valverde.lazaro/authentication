package redis

import (
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/domain"
)

const (
	expirationTime = 10 * time.Minute
)

//Service represent redis service
type Service struct {
	client *redis.Client
}

//NewService represents new redis service
func NewService(config *Config) *Service {
	return &Service{
		client: redis.NewClient(&redis.Options{
			Addr:     config.Addr,
			Password: config.Password,
			DB:       config.DB,
		}),
	}
}

//PersistToken persist token in redis
func (s *Service) PersistToken(auth *domain.AuthToken) error {
	return s.client.Set(auth.Username, auth.Token, expirationTime).Err()
}

// RetrieveToken retrieve an existing token
func (s *Service) RetrieveToken(username string) (string, error) {
	return s.client.Get(username).Result()
}
