package mongodb

import (
	"context"
	"time"

	"gitlab.com/jozef.valverde.lazaro/authentication/internal/domain"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	mongoURI   = "mongodb://localhost:27017"
	dbTesting  = "test"
	collection = "user"
)

//Service define mongo service
type Service struct {
	client *mongo.Client
}

//NewService represent new mongo service
func NewService() *Service {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	if err != nil {
		panic(err)
	}
	return &Service{
		client: client,
	}
}

//GetUserByUsername find user by username
func (s *Service) GetUserByUsername(username string) (*domain.UserFindMongo, error) {
	var user *domain.UserFindMongo
	collection := s.client.Database(dbTesting).Collection(collection)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := collection.FindOne(ctx, bson.M{"username": username}).Decode(&user)
	if err != nil {
		return nil, err
	}
	return user, nil
}
