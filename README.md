# Authentication

This microservice allows to authenticate users

### Mongo
```
mkdir ~/data
sudo docker run -d -p 27017:27017 -v ~/data:/data/db mongo
```
### Redis
Here we're giving it a name (redis) and exposing port 6379 (the Redis default)
```
docker run -d -p 6379:6379 --name redis redis
```
Check it's running with
```
docker ps
```
And view the log output with
```
docker logs redis
```

### Run the Redis CLI
We're going to start a new interactive session (-it) inside the running container
```
docker exec -it redis sh
```
And now we're attached to our container. Let's run redis-cli:
```
redis-cli
```

### Try out some basic Redis commands
If we send a "ping", should get "PONG" back:
```
127.0.0.1:6379> ping
PONG
```
Try out some more commands (set a key and increment a counter)
```
127.0.0.1:6379> set name mark
OK
127.0.0.1:6379> get name
"mark"
127.0.0.1:6379> incr counter
(integer) 1
127.0.0.1:6379> incr counter
(integer) 2
127.0.0.1:6379> get counter
"2"
```
And when we're done exit out of redis-cli and sh:
```
127.0.0.1:6379> exit
# exit
```