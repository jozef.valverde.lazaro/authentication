package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/rest"
)

func main() {
	engine := gin.Default()
	router := rest.NewRouter(engine)
	router.URLMapping()
}
