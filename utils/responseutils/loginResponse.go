package responseutils

import (
	"gitlab.com/jozef.valverde.lazaro/authentication/internal/domain"
)

//NewTokenLoginResponse return new login response
func NewTokenLoginResponse(accessToken string, refreshToken string) *domain.TokenLoginResponse {
	return &domain.TokenLoginResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}
