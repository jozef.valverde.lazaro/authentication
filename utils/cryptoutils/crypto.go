package cryptoutils

import (
	"crypto/md5"
	"encoding/hex"
)

//Encrypt inputs
func Encrypt(input string) string {
	hash := md5.New()
	defer hash.Reset()
	hash.Write([]byte(input))
	return hex.EncodeToString(hash.Sum(nil))
}
